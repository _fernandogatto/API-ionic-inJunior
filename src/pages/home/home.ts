import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { SesionsProvider } from '../../providers/sesions/sesions';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController, 
    public sessions: SesionsProvider,
    public app: App
  ) {

  }

  logout(): void {
    this.sessions.logout();
    this.app.getRootNav().push(LoginPage);
  }

  /* IDEIA DE PILHA
  push() - vai para a proxima pagina
  pop() - volta para a pagina anterior
  setRoot() - zera a pilha
  */
}
